class Cliente {
    private String nombre;
    private String email;

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;

        System.out.println("Estoy en setNombre");
        System.out.println(this.nombre);
    }


    public String getEmail() {
	return this.email;
    }


    public void setEmail(String email) {
	this.email = email;
    }



}
