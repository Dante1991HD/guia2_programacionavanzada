
public class Persona {
	
	String nombre;
	String apellido;
	String estado;
	Viste viste;
	
	Persona(String param_nombre, String param_apellido){
		this.nombre = param_nombre;
		this.apellido = param_apellido;
		this.estado = "Descansando";
		
		System.out.println("Constructor de 2 parámetros");
	}
	
	Persona(String param_nombre){
		
		this.nombre = param_nombre;
		System.out.println("Constructor de 1 parámetro");
	}
	
	
	
	void decir_nombre () {
		
		System.out.println("Hola, mi nombre es " + this.nombre);
		
	}
	
	void decir_apellido() {
		
		System.out.println("Mi apellido es " + this.apellido);
		
	}
	
	String correr () {
		
		this.estado = "Hola soy " + this.nombre + " Mi estado actual es Corriendo";
		
		return this.estado;
	}
	
	String durmiendo(){
		this.estado = "Hola soy " + this.nombre + " Mi estado actual es Durmiendo";
		
		return this.estado;
	}
	
	void vestir(Pantalon param_pantalon, Polera param_polera) {
		
		this.viste = new Viste(param_pantalon, param_polera);
		
	}
	
}
