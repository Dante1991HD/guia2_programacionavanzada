
public class Tarjeta {
	String nombre;
	int saldo;
	
	Tarjeta(String param_nombre, int param_saldo){
		this.nombre = param_nombre;
		this.saldo = param_saldo;
	}
	void actualizar_saldo(int new_saldo) {
		this.saldo = new_saldo;
	}
	void muestrar_saldo() {
		System.out.println(this.saldo);
	}
}
