
public class Perro {
	String color;
	String raza;
	int edad;
	char sexo;
	float peso;
	
	Perro(String param_color, String param_raza, int param_edad, char param_sexo, float param_peso){
		this.color = param_color;
		this.raza = param_raza;
		this.edad = param_edad;
		this.sexo = param_sexo;
		this.peso = param_peso;
		
	}
 
	void ladrar() {
		System.out.printf("Le can de raza %s, de color %s se encuentra ladrando...\n", this.raza, this.color);
		
	}
	 
	void jugar() {
		if (this.edad < 7) System.out.printf("Le can de %d años es bastante juguetón\n", this.edad);
		else if (this.edad < 10) System.out.printf("Le can de %d años aún es juguetón para su edad\n", this.edad);
		else System.out.printf("Le can de %d años aún se esfuerza por jugar :')\n", this.edad);

	}
	
	void comer() {
		if (this.peso < 10) System.out.println("Le can está comiendo, mejor no molestar");
		else System.out.println("Le can se esfuerza por respirar mientras come");

	}
	
	void pelear() {
		if (this.sexo == 'M')	System.out.println("Les canes se están peleando");
		else System.out.println("Las hembras no pelean según guía");

	}
}