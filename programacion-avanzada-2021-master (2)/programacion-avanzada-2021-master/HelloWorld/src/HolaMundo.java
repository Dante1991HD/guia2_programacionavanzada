
public class HolaMundo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Hola Mundo");
		
		Persona persona1;
		
		persona1 = new Persona("Pepito", "Pepon");
		persona1.decir_nombre();
		String estado = persona1.correr();
		System.out.println(estado);

		
		Persona persona2;
		persona2 = new Persona("Luna");
		persona2.decir_nombre();
		persona2.decir_apellido();
		
		persona2.apellido = "Maria";
		persona2.decir_apellido();
		
		estado = persona2.durmiendo();
		System.out.println(estado);
		
		
		Pantalon pantalon1;
		Pantalon pantalon2;
		Pantalon pantalon3;
		
		pantalon1 = new Pantalon();
		pantalon1.tipo = "Corto";
		
		pantalon2 = new Pantalon();
		pantalon2.tipo = "Jeans";
		
		pantalon3 = new Pantalon();
		pantalon3.tipo = "Vestido";
		
		Polera polera1 = new Polera();
		Polera polera2 = new Polera();
		
		polera1.color = "Azul";
		polera2.color = "Rojo";
	
		Viste viste_p1;
		viste_p1 = new Viste(pantalon1, polera1);
		persona1.viste = viste_p1;
		
		System.out.println(persona1.viste.pantalon.tipo);
		System.out.println(persona1.viste.polera.color);
		
		
		persona2.vestir(pantalon3, polera1);
		System.out.println(persona2.viste.pantalon.tipo);
		System.out.println(persona2.viste.polera.color);
		
	}

}
