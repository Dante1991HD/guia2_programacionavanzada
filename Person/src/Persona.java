
public class Persona {
	String nombre;
	String apellido;
	String estado;
	
	Persona(String param_nombre, String param_apellido){
		
		this.nombre = param_nombre;
		this.apellido = param_apellido;
		this.estado = "Descansado";
	}
	
	void muestra_nombre () {
		
		System.out.println(this.nombre);
		
	}
	
	String correr () {
		
		this.estado = "Corriendo";
		
		return this.estado;
	}
}
